//
//  AjoutPastryViewController.swift
//  KoOoL
//
//  Created by houssem on 5/7/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class AjoutPastryViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var nom: UITextField!
    
    @IBOutlet weak var tel: UITextField!
    @IBAction func btnAjoutPastry(_ sender: Any) {
        
    }
    @IBOutlet weak var bigDesc: UITextView!
    @IBOutlet weak var adresse: UITextField!
    @IBOutlet weak var smallDesc: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    var idpastry = 0
    var imagePicker = UIImagePickerController()
    var stringImage = ""
    var testImage = false
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //let image = info as! UIImage
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            let imageData = imageView.image?.jpegData(compressionQuality: 0.5)
            stringImage = imageData!.base64EncodedString()
            self.testImage = true
            
        }else  {
            fatalError("exception pour l'imae")
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnaction(_ sender: Any) {
        let urlString = ViewController.AdresseIp.monAdresse + "/api/tdescriptions"
        Alamofire.request(urlString, method: .post, parameters: [
            "bigdesc": bigDesc.text!,
            "smalldesc": smallDesc.text!,
            ],encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                switch response.result {
                case .success:
                    
                    let pastry = response.result.value as!  Dictionary<String, Any>
                    let idpastr = pastry["id"] as!  Int
                    let legende = self.randomStringWithLength(len: 6)
                    //let legende = Float.random(in: 0...100000000000)
                    self.ajoutPastry(id: idpastr,legende: String(legende))
                    
                    
                    break
                case .failure(let error):
                    print("ajout pastry description class, jason responce eroor ")
                    print(error)
                }
        }
    }
    
    @IBAction func btnAjoutImage(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func ajoutPastry(id : Int , legende : String){
        // commencer par les test et finir avec lajout
        
        if self.nom.text!.count == 0{
            // non ne va pas
            self.nom.layer.borderWidth = 1.0
            self.nom.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
        }else{
            if tel.text!.count == 0 || !tel.text!.isInt{
                //tel ne vas pas
                tel.layer.borderWidth = 1.0
                tel.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
            }else{
                if adresse.text!.count == 0{
                    // adresse ne va pas
                    adresse.layer.borderWidth = 1.0
                    adresse.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
                }else{
                    if smallDesc.text.count == 0 {
                        //small desc ne va pas
                        smallDesc.layer.borderWidth = 1.0
                        smallDesc.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
                    }else{
                        if bigDesc.text.count == 0 {
                            // big desc ne va pas
                            bigDesc.layer.borderWidth = 1.0
                            bigDesc.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
                        }else{
                            if !testImage{
                                // lutilisateur na pas mi d image
                                imageView.layer.borderWidth = 3.0
                                imageView.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
                            }else{
                                let urlString = ViewController.AdresseIp.monAdresse + "/api/tpastries"
                                Alamofire.request(urlString, method: .post, parameters: [
                                    "adresse": adresse.text!,
                                    "nom": nom.text!,
                                    "image":legende,
                                    "tel": tel.text!,
                                    "iddesc": id,
                                    ],encoding: JSONEncoding.default, headers: nil).responseJSON {
                                        response in
                                        switch response.result {
                                        case .success:
                                            let pastry = response.result.value as!  Dictionary<String, Any>
                                            self.idpastry = pastry["id"] as!  Int
                                            self.ajouterIdauUser(id: self.idpastry)
                                            GererPastryViewController.idaffectaion = self.idpastry
                                            self.uploadImage(legende: String(legende))
                                            
                                            break
                                        case .failure(let error):
                                            print("ajout pastry class, jason responce eroor ")
                                            print(error)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    
    func ajouterIdauUser(id : Int) {
        let IduserTemp = String(ProfileViewController.idUser)
        let urlString = ViewController.AdresseIp.monAdresse + "/api/tusers/" + IduserTemp
        
        ProfileViewController.userConnecte.idpastry = id
        print(ProfileViewController.userConnecte)
        let resseources :[String : Any] = [
            "email" : ProfileViewController.userConnecte.email,
            "nom": ProfileViewController.userConnecte.nom,
            "password": ProfileViewController.userConnecte.password,
            "role": ProfileViewController.userConnecte.role,
            "tel": ProfileViewController.userConnecte.tel,
            "id": ProfileViewController.userConnecte.id,
            "idpastry": id
        ]
        Alamofire.request(urlString, method: HTTPMethod.put, parameters:resseources,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                break
            case .failure(let error):
                print(" -=-=-=-=-=-=-=- UPDATE user Eror, jason responce eroor -=-=-=-=-=-=-=-")
                print(error)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail"{
            let destination = segue.destination as! GererPastryViewController
            
            destination.id = idpastry as! Int
        }
    }
    func uploadImage(legende : String)  {
        
        
        
        print("entrer dans la methode ")
        //print(stringImage)
        
        let urlS =  "http://41.226.11.252:1180/koool/uploadImage.php"
        Alamofire.request(urlS, method: .post, parameters: [
            "image": stringImage,
            "legende": legende ,
            ]).responseJSON {
                response in
                
                print(response.result.value)
                print(" sucess ajout image pastry  class,")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                self.performSegue(withIdentifier: "detail",sender: Any?.self)
        }
        
        
    }
    
    func randomStringWithLength(len: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<len).map{ _ in letters.randomElement()! })
    }

}


