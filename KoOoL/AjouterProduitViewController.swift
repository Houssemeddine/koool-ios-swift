//
//  AjouterProduitViewController.swift
//  KoOoL
//
//  Created by Ahmed on 5/8/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class AjouterProduitViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource , UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var test  = false
    var imagePicker = UIImagePickerController()
    var stringImage = ""
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //let image = info as! UIImage
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            let imageData = imageView.image?.jpegData(compressionQuality: 0.5)
            stringImage = imageData!.base64EncodedString()
            self.test = true
            
        }else  {
            fatalError("exception pour l'imae")
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    @IBAction func btnAjoutImage(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    var tempTypeeee = ""
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tempTypeeee = pickerData[row]
    }
    
    
    // mes outlets
    @IBOutlet weak var nomProduitOutlet: UITextField!
    @IBOutlet weak var typeProduitPikerViewOutlet: UIPickerView!
    @IBOutlet weak var prixProduitOutlet: UITextField!
    
    @IBOutlet weak var petitDescriptionOutlet: UITextView!
    @IBOutlet weak var grandeDescriptionOutlet: UITextView!
    
    var unId = 0
    
    var pickerData: [String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.typeProduitPikerViewOutlet.delegate = self
        self.typeProduitPikerViewOutlet.dataSource = self
        pickerData = ["gateau", "jus", "salée", "soiree", "cake", "shour"]
    }
    @IBAction func BtnAjouter(_ sender: Any) {
        let nomProduit = nomProduitOutlet.text as? String
        let typeProduit = tempTypeeee
        let prixProduit = prixProduitOutlet.text as? String
        let grandeDescriptionProduit = grandeDescriptionOutlet.text as? String
        let petiteDescriptionProduit = petitDescriptionOutlet.text as? String
        
        if nomProduit!.count == 0{
            nomProduitOutlet.layer.borderWidth = 1.0
            nomProduitOutlet.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
        }else{
            if prixProduit!.count == 0 || !prixProduit!.isInt{
                prixProduitOutlet.layer.borderWidth = 1.0
                prixProduitOutlet.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
            }else{
                if petiteDescriptionProduit!.count == 0 {
                    petitDescriptionOutlet.layer.borderWidth = 1.0
                    petitDescriptionOutlet.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
                    
                }else{
                    if grandeDescriptionProduit!.count == 0{
                        grandeDescriptionOutlet.layer.borderWidth = 1.0
                        grandeDescriptionOutlet.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
                    }else{
                        //test sur limage
                        if self.test{ // il a choistit une image
                            print(self.imageView.image)
                            let urlString = ViewController.AdresseIp.monAdresse + "/api/tdescriptions"
                            Alamofire.request(urlString, method: .post, parameters: [
                                "bigdesc": grandeDescriptionProduit!,
                                "smalldesc":petiteDescriptionProduit!
                                ], encoding: JSONEncoding.default, headers: nil).responseJSON{
                                    response in
                                    let ladescription = response.result.value as! Dictionary<String, Any>
                                    let idNouvelleDescription = ladescription["id"] as! Int
                                    let prixTemp = Int(prixProduit!)
                                    
                                    let legende = self.randomStringWithLength(len: 6)
                                    self.uploadImage(legende: legende)
                                    self.ajouterProd(idDes: idNouvelleDescription, nom: nomProduit!, prix: prixTemp!, image: legende, idpast: GererPastryViewController.idaffectaion, type2: self.tempTypeeee)
                            }
                        }else{// in n'a pas choist dimage
                            let urlString = ViewController.AdresseIp.monAdresse + "/api/tdescriptions"
                            Alamofire.request(urlString, method: .post, parameters: [
                                "bigdesc": grandeDescriptionProduit!,
                                "smalldesc":petiteDescriptionProduit!
                                ], encoding: JSONEncoding.default, headers: nil).responseJSON{
                                    response in
                                    let ladescription = response.result.value as! Dictionary<String, Any>
                                    let idNouvelleDescription = ladescription["id"] as! Int
                                    let prixTemp = Int(prixProduit!)
                                    
                                    self.ajouterProd(idDes: idNouvelleDescription, nom: nomProduit!, prix: prixTemp!, image: "nondispo", idpast: GererPastryViewController.idaffectaion, type2: self.tempTypeeee)
                            }
                            
                        }
                        
                    }
                }
            }
        }
    }
    func ajouterProd(idDes : Int, nom : String, prix: Int, image : String, idpast: Int, type2: String) {
        let urlPostProduit = ViewController.AdresseIp.monAdresse + "/api/tproduits"
        var params = [
            "nom": nom,
            "prix": prix,
            "iddesc":idDes,
            "image": image,
            "idpastry": idpast,
            "type": type2
            ] as [String : Any]
        Alamofire.request(urlPostProduit, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON{
            response in
            switch response.result {
            case .success:
                
               self.performSegue(withIdentifier: "endaaaaah",sender: Any?.self)
                break
            case .failure(let error):
                print("ajoutProduit class, jason responce eroor ")
                print(error)
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "endaaaaah"{
        }
    }
    func uploadImage(legende : String)  {
        
        
        
        print("entrer dans la methode ")
        //print(stringImage)
        
        let urlS =  "http://41.226.11.252:1180/koool/uploadImage.php"
        Alamofire.request(urlS, method: .post, parameters: [
            "image": stringImage,
            "legende": legende ,
            ]).responseJSON {
                response in
                
        }
        
        
    }
    
    func randomStringWithLength(len: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<len).map{ _ in letters.randomElement()! })
    }

    
    
    var idProduit = 0
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
extension String {
    var isInt: Bool {
        return Int(self) != nil
    }
}
