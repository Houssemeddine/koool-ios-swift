//
//  CakeViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/10/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CakeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tabRecents: UICollectionView!
    
    @IBAction func chercher(_ sender: UITextField) {
        tabRecents.isHidden = true
        var height:CGFloat = 0
        for view in self.view.subviews {
            height = height + view.bounds.size.height
        }
    }
    
    @IBOutlet weak var tab: UITableView!
    var patisseries : NSArray = []
    var les10derniers : NSArray = []
    
    
    // cette methode va nous permettre d`afficher les icone en bas      qh
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem (title: "Cake" , image: UIImage(named: "iconcake") , tag: 1)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = ViewController.AdresseIp.monAdresse + "/api/tpastries"
        Alamofire.request(url).responseJSON{ response in
            self.patisseries = response.result.value as! NSArray
            self.les10derniers = self.getLastFive(tabInitial: self.patisseries)
            self.tabRecents.reloadData()
            self.tab.reloadData()
        }
        
        // ce cui est pour le bouton rafrechir
        let refrech = UIBarButtonItem(barButtonSystemItem: .refresh  , target: self, action: #selector(leAction))
        self.navigationItem.rightBarButtonItems = [refrech]

        
        // ce ci est pour recharger la page apres avoir ajouter une patisserie
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
    }
    
    //ces deux methode nous permette de remlir la collectionview des dernier patisserie ajouter
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.les10derniers.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellrecent", for: indexPath)
        let imageview = cell.viewWithTag(10) as! UIImageView
        
        
        let patiss = les10derniers[indexPath.row] as! Dictionary<String, Any>
        let iUrl = patiss["image"] as! String
        let format = ".jpg"
        let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
        imageview.af_setImage(withURL: URL(string: imageUrl)!)
        
        return cell
    }

    //ce ci permet de remplir le table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patisseries.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellparcour")
        let imageview = cell!.viewWithTag(20) as! UIImageView
        let labelNom = cell!.viewWithTag(21) as! UILabel
        let labeldescription = cell!.viewWithTag(22) as! UITextView
        
        let patiss = patisseries[indexPath.row] as! Dictionary<String, Any>
        labelNom.text = patiss["nom"] as? String
        
        let iUrl = patiss["image"] as! String
        let format = ".jpg"
        let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
        imageview.af_setImage(withURL: URL(string: imageUrl)!)
        
        let descriptionText = patiss["iddesc"] as! Dictionary<String, Any>
        labeldescription.text = descriptionText["smalldesc"] as? String
        
        
        return cell!
        
    }
    
    //pour les navigation
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toDetail1", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toDetail", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail"{
            let destination = segue.destination as! DetailViewController
            let indice = sender as! IndexPath
            let patiss = patisseries[indice.row] as! Dictionary<String, Any>
            destination.id = patiss["id"] as! Int
        }
        if segue.identifier == "toDetail1"{
            let destination = segue.destination as! DetailViewController
            let indice = sender as! IndexPath
            let patiss = les10derniers[indice.row] as! Dictionary<String, Any>
            destination.id = patiss["id"] as! Int
        }
    }
    
    //pour avoir les cinque dernier elemnt de la table
    func getLastFive(tabInitial : NSArray) -> NSArray {
        var res : NSMutableArray = []
        var temRes = tabInitial
        var ourourou = temRes.mutableCopy() as! NSMutableArray
        for index in 1...6 {
            res.add(ourourou.lastObject)
            ourourou.removeLastObject()
        }
        return res
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // ce ci est pour recharger la page apres avoir ajouter une patisserie
    
    
    @objc func loadList(){
        //load data here
        self.tabRecents.reloadData()
        self.tab.reloadData()
    }
    @objc func leAction(){
        let url = ViewController.AdresseIp.monAdresse + "/api/tpastries"
        Alamofire.request(url).responseJSON{ response in
            self.patisseries = response.result.value as! NSArray
            self.les10derniers = self.getLastFive(tabInitial: self.patisseries)
            self.tabRecents.reloadData()
            self.tab.reloadData()
        }
    }
    

}

