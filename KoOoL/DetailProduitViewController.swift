//
//  DetailProduitViewController.swift
//  KoOoL
//
//  Created by Ahmed on 5/1/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import Alamofire

class DetailProduitViewController: UIViewController {
    public static var id = 0
    
    //declaration des outlet
    @IBOutlet weak var str5: UIImageView!
    @IBOutlet weak var str4: UIImageView!
    @IBOutlet weak var str3: UIImageView!
    @IBOutlet weak var str2: UIImageView!
    @IBOutlet weak var str1: UIImageView!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imagevieew: UIImageView!
    @IBOutlet weak var nomlabel: UILabel!
    @IBOutlet weak var typelabel: UILabel!
    @IBOutlet weak var venduParLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var prixLabel: UILabel!
    
    @IBOutlet weak var viezRate: UIView!
    @IBOutlet weak var valeurNote: UILabel!
    
    @IBAction func spinnerGetNote(_ sender: UISlider) {
        let tempNote = spinner.value * 5
        valeurNote.text = String(tempNote.description)
    }
    @IBOutlet weak var spinner: UISlider!
    @IBAction func validerNoteAction(_ sender: UIButton) {
        if !testNoterAvant(){
            print(testNoterAvant())
            let tempNote = spinner.value * 5
            let urlString = ViewController.AdresseIp.monAdresse + "/api/tnotes"
            Alamofire.request(urlString, method: .post, parameters: [
                "note": tempNote,
                "iduser": ProfileViewController.idUser,
                "idproduit": DetailProduitViewController.id
                ],encoding: JSONEncoding.default, headers: nil).responseData {
                    response in
                    switch response.result {
                    case .success:
                        self.viewDidLoad()
                        break
                    case .failure(let error):
                        print("regester class, jason responce eroor ")
                        print(error)
                    }
            }
        }else{
            print("ce ci est le else")
            let alert = UIAlertController(title: "Attention !", message: "Vous avez deja noter ce produit", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    //fin des outlet
    
    var produit :  Dictionary<String, Any> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        if ProfileViewController.idUser == 0 {
            viezRate.isHidden = true
        }

        // Do any additional setup after loading the view.
        let url = ViewController.AdresseIp.monAdresse + "/api/tproduits/"+String(DetailProduitViewController.id)
        Alamofire.request(url).responseJSON{ response in
            self.produit = response.result.value as! Dictionary<String, Any>
            //self.viewtoreload.setNeedsLayout()
            let iUrl = self.produit["image"] as! String
            let format = ".jpg"
            let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
            self.imagevieew.af_setImage(withURL: URL(string: imageUrl)!)
            self.imgBg.af_setImage(withURL: URL(string: imageUrl)!)

            
            self.nomlabel.text = self.produit["nom"] as? String
            self.typelabel.text = self.produit["type"] as? String
            let descriptionText = self.produit["iddesc"] as! Dictionary<String, Any>
            self.descriptionLabel.text = descriptionText["bigdesc"] as? String
            
            let vendu = self.produit["idpastry"] as! Dictionary<String, Any>
            let chien = vendu["nom"] as? String
            let vv = "Proposé par  " + chien!
            self.venduParLabel.text = vv
            
            let temp = String(self.produit["prix"] as! Int) + " DT"
            self.prixLabel.text = temp

        }
        
        // ce qui vien la est relative au note
        self.str1.isHidden = true
        self.str2.isHidden = true
        self.str3.isHidden = true
        self.str4.isHidden = true
        self.str5.isHidden = true
        self.getNbStar()
    }
    
    var ttLesNote : NSArray = []
    
    func getNbStar() {
        var nombreAvis = 0
        var totalAvis = 0
        
        self.str1.isHidden = true
        self.str2.isHidden = true
        self.str3.isHidden = true
        self.str4.isHidden = true
        self.str5.isHidden = true
        
        let urlString = ViewController.AdresseIp.monAdresse + "/api/tnotes"
        Alamofire.request(urlString).responseJSON{ response in
            
            if let kaka = response.result.value {
                self.ttLesNote = kaka as! NSArray
            for elem in self.ttLesNote{
                let element = elem as! Dictionary<String, Any>
                let idElementtemp = element["idproduit"] as! Dictionary<String, Any>
                let idElement = idElementtemp["id"] as! Int
                if idElement == DetailProduitViewController.id{
                    nombreAvis = nombreAvis + 1
                    let tempval = element["note"] as! Int
                    totalAvis = totalAvis + tempval
                    let moyenne = totalAvis/nombreAvis
                    
                    if moyenne > 1 || moyenne == 1 { self.str1.isHidden = false }
                    if moyenne > 2 || moyenne == 2 { self.str2.isHidden = false }
                    if moyenne > 3 || moyenne == 3 { self.str3.isHidden = false }
                    if moyenne > 4 || moyenne == 4 { self.str4.isHidden = false }
                    if moyenne > 5 || moyenne == 5 { self.str5.isHidden = false }
                    
                }
            }
        }
        }
        }
    func testNoterAvant() -> Bool {
        for temp in self.ttLesNote{
            let element = temp as! Dictionary<String, Any>
            
            let produit = element["idproduit"] as! Dictionary<String, Any>
            let idproduit = produit["id"] as! Int
            
            let users = element["iduser"] as! Dictionary<String, Any>
            let idUser = users["id"] as! Int
            
            if idUser == ProfileViewController.idUser && idproduit == DetailProduitViewController.id{
                return true
            }
        }
        return false
    }
    }
extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
    
    
    // fonction pour savoir si le user a deja niter ce produit ou pas

}
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
