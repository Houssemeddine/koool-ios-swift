//
//  DetailViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/16/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import CoreData

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var btnPoster: UIButton!
    @IBOutlet weak var nbSubLabel: UILabel!
    @IBOutlet weak var tab: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nomLabel: UILabel!
    @IBOutlet weak var telLabel: UILabel!
    @IBOutlet weak var descriptionArea: UITextView!
    @IBOutlet weak var commentArea: UITextField!
    @IBOutlet weak var adresseLabel: UILabel!
    var id = 0   
    @IBOutlet weak var viewtoreload: UIView!
    
    var patisserieResponse : NSArray = []
    var allSubscribs : NSArray = []
    var patisserie : Dictionary<String, Any> = [:]
    var commentaires : Array<Comment> = Array()

    @IBOutlet weak var btnfav: UIButton!
    @IBOutlet weak var textcommentaire: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // check if the user is connected or not
        if ProfileViewController.idUser == 0{
            btnfav.isHidden = true
            btnPoster.isHidden = true
        }
        
        // Do any additional setup after loading the view.
        let url = ViewController.AdresseIp.monAdresse + "/api/tpastries/"+String(id)
        Alamofire.request(url).responseJSON{ response in
            self.patisserie = response.result.value as! Dictionary<String, Any>
            self.viewtoreload.setNeedsLayout()
            self.nomLabel.text = self.patisserie["nom"] as? String
            self.telLabel.text = String(self.patisserie["tel"] as! Int)
            self.adresseLabel.text =  self.patisserie["adresse"] as? String
            let descriptionText = self.patisserie["iddesc"] as! Dictionary<String, Any>
            self.descriptionArea.text = descriptionText["bigdesc"] as? String
            let iUrl = self.patisserie["image"] as! String
            let format = ".jpg"
            let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
            self.imageView.af_setImage(withURL: URL(string: imageUrl)!)
            print(imageUrl)
        }
        getsub()
         getAllComment(id: self.id)
        
        self.viewtoreload.setNeedsLayout()
        
    }
    @IBAction func favoriser(_ sender: UIButton) {
        let urlString = ViewController.AdresseIp.monAdresse + "/verifierAbonnerPastry"
        Alamofire.request(urlString,parameters: [
            "pastry": self.id,
            "user": ProfileViewController.idUser
            ]).responseJSON{ response in
                let tempResultat = response.result.value as! Dictionary<String, Bool>
                let aaa  = tempResultat["subscribe"] as! Bool
                if !aaa{
                    self.sabonner()
                }
        }
    }
    @IBAction func addComment(_ sender: UIButton) {
    }
    
    //cette methode nous permet de savoir si la patisserie est dans nos favorie ou nn avant de l'ajouter
    func sabonner(){
        let urlString = ViewController.AdresseIp.monAdresse + "/api/tsubscribes"
        Alamofire.request(urlString, method: .post, parameters: [
            "idpastry": self.id,
            "iduser": ProfileViewController.idUser,
            "idproduit": 4
            ],encoding: JSONEncoding.default, headers: nil).responseData {
                response in
                switch response.result {
                case .success:
                    let alert = UIAlertController(title: "Bien", message: "tu t'es abonné.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    self.getsub()
                    break
                case .failure(let error):
                    print("detail class.swift, jason responce eroor ")
                    print(error)
                }
        }
    }
    
    @IBAction func afiicherProduit(_ sender: UIButton) {
        performSegue(withIdentifier: "afficherProduits", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "afficherProduits"{
            let destination = segue.destination as! ProduitPatisserieViewController
            destination.id = String(self.id)
        }
    }
    
    func getsub()  {
        let urlNbSubs = ViewController.AdresseIp.monAdresse + "/SumAabonnerPastry"
        Alamofire.request(urlNbSubs,parameters: [
            "idpastry": self.id
            ]).responseJSON{ response in
                let tempResultat = response.result.value as! Dictionary<String, Int>
                self.nbSubLabel.text  = String(tempResultat["somme"] as! Int)
        }
    }
    
    // pour les commentaires
    
    
    struct Comment {
        var id : Int
        var descriptionText : String
        var idpastry : Int
        var user : String
        var datePublication : String
        init(){
            self.id = 0
            self.idpastry = 0
            self.user = ""
            self.descriptionText = ""
            self.datePublication = ""
        }
    }
    
    
    var commentairesNSArr : NSArray = []
    // les methodes seront get all comment de la patisseire
    
    func getAllComment (id : Int){
        let url = ViewController.AdresseIp.monAdresse + "/api/tcommentaires"
        Alamofire.request(url).responseJSON{ response in
            self.commentairesNSArr = response.result.value as! NSArray
            for c in self.commentairesNSArr{
                let comm = c as! Dictionary<String, Any>
                
                let patTemp = comm["idpastry"] as! Dictionary<String, Any>
                let var1 = patTemp["id"] as! Int
                
                if var1 == id{
                    
                    var temp = Comment()
                    temp.id = id
                    
                    let userTemp = comm["iduser"] as! Dictionary<String, Any>
                    temp.user = userTemp["nom"] as! String
                    
                    let patTemp = comm["idpastry"] as! Dictionary<String, Any>
                    temp.idpastry = patTemp["id"] as! Int
                    temp.descriptionText = comm["comentaire"] as! String
                    
                    //let test2 = comm["date"] as! String
                    //temp.datePublication = test2
                    
                    self.commentaires.append(temp)
                    self.viewtoreload.setNeedsLayout()
                    self.tab.reloadData()
                    
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentaires.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCommentaire")
        let labelNom = cell!.viewWithTag(56) as! UILabel
        //let labeldate = cell!.viewWithTag(57) as! UILabel
        let labeldcomm = cell!.viewWithTag(58) as! UITextView
        
        labelNom.text = commentaires[indexPath.row].user
        //labeldate.text = commentaires[indexPath.row].datePublication
        labeldcomm.text = String(commentaires[indexPath.row].descriptionText)
        
        //affectation de la cpouleur au contour de la cell
        cell!.layer.borderWidth = 1.0
        cell!.layer.borderColor = UIColor(hexString: "#C2185B ").cgColor
        
        return cell!
    }
    
    @IBAction func posterCommentaire(_ sender: UIButton) {
        let txt = textcommentaire.text!
        if txt.count > 0 {
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            let myString = formatter.string(from: Date()) // string purpose I add here
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            //then again set the date format whhich type of output you need
            formatter.dateFormat = "yyyy-MM-dd T HH:mm:ss"
            // again convert your date to string
            let myStringafd = formatter.string(from: yourDate!)
            
            print(myStringafd)
            let urlString = ViewController.AdresseIp.monAdresse + "/api/tcommentaires"
            Alamofire.request(urlString, method: .post, parameters: [
                "comentaire": txt,
                "iduser": ProfileViewController.idUser,
                "idpastry": self.id/*,
                "date":  Date()*/
 
                ],encoding: JSONEncoding.default, headers: nil).responseData {
                    response in
                    switch response.result {
                    case .success:
                        self.getAllComment(id: self.id)
                        self.textcommentaire.text = ""
                        self.tab.reloadData()
                        break
                    case .failure(let error):
                        print("regester class, jason responce eroor ")
                        print(error)
                    }
            }
        }else{
            
        }
    }
    
    //plus tard
    /*
     let dateString = dict["now_in"]!["date"] // comm["date"] as! String
     
     let format = "yyyy-MM-dd'T'HH:mm:ss"
     let formatter = DateFormatter()
     formatter.dateFormat = format
     let date = formatter.date(from: dateString)
     
     */
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
}
