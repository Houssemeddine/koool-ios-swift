//
//  FavorisViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/21/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import AlamofireImage

class FavorisViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate {
    var arrayCoreData = [String]()
    struct elem {
        var id : String
        var nom : String
        var tel : String
        var image : String
        init(){
            self.id = ""
            self.nom = ""
            self.tel = ""
            self.image = ""
        }
    }
    var arrayElement = [elem]()

    @IBOutlet weak var colP: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //rrecuperation du core data
        getCoreData()
        getAlamoFire()
    }

    
    //methode de recuperation des favoris
    func getCoreData(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let reqete = NSFetchRequest<NSFetchRequestResult>(entityName: "Patisserie")
        reqete.returnsObjectsAsFaults = false
        do {
            let resultats = try context.fetch(reqete)
            if resultats.count > 0{
                for r in resultats as! [NSManagedObject]{
                    if let id = r.value(forKey: "id") as? String{
                        arrayCoreData.append(id)
                        print(id)
                    }
                }
            }
        } catch  {
            print("eror dans le get de l appli")
        }
    }
    // methode pour getter tt les favorie des web services
    func getAlamoFire(){
        let url = ViewController.AdresseIp.monAdresse + "/api/tpastries/"
        for idPatisserie in arrayCoreData.enumerated() {
            let urlTemp = url + String(idPatisserie.element)
            Alamofire.request(urlTemp).responseJSON{ response in
                var temp = elem()
                let patisserie = response.result.value as! Dictionary<String, Any>
                temp.nom = patisserie["nom"] as! String
                temp.tel = String(patisserie["tel"] as! Int)
                let iUrl = patisserie["image"] as! String
                let format = ".jpg"
                temp.image = ViewController.AdresseIp.adresseImage + iUrl + format
                temp.id = idPatisserie.element
                self.arrayElement.append(temp)
                self.colP.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayElement.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellPat", for: indexPath)
        let imageView = cell.viewWithTag(11) as! UIImageView
        let nomLabel = cell.viewWithTag(12) as! UILabel
        let telLabel = cell.viewWithTag(13) as! UILabel
        nomLabel.text = arrayElement[indexPath.row].nom
        telLabel.text = arrayElement[indexPath.row].tel
        imageView.af_setImage(withURL: URL(string: arrayElement[indexPath.row].image)!)
        return cell
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
