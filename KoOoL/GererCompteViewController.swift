//
//  GererCompteViewController.swift
//  KoOoL
//
//  Created by Ahmed on 5/13/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import Alamofire

class GererCompteViewController: UIViewController {
    //il faut :
    // 0- avoir les tt les outlets
    // 1- enlever le champs patente de la vue
    // 2- l'afficher en liquqnt sur le btn afficher patente et hider le btn
    // 3- remplir tt les champs avec les information de l'utilisateur
    // 4- tester toute les valeur des champs
    // 5- updater dans la base de donnees
    // 6- redirection vers acceil
    // remarque : en cas de changement du password il faut verifier que l'anciien est correcte
    
    // 0)
    @IBOutlet weak var nomOutlet: UITextField!
    @IBOutlet weak var telOutlet: UITextField!
    @IBOutlet weak var adresseOutlet: UITextField!
    @IBOutlet weak var ancienMDPoutlet: UITextField!
    @IBOutlet weak var nouveaupassOutlet: UITextField!
    @IBOutlet weak var confirmPassOutlet: UITextField!
    @IBOutlet weak var patenteOutlet: UITextField!
    @IBOutlet weak var btnAfficherOutlet: UIButton!
    @IBOutlet weak var emailOutlet: UITextField!
    
    var ancienMdp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 1)
        patenteOutlet.isHidden = true
        
        // 3)
        let tempIdUser = String(ProfileViewController.idUser)
        let url = ViewController.AdresseIp.monAdresse + "/api/tusers/" + tempIdUser
        Alamofire.request(url).responseJSON{ response in
            let leUser = response.result.value as! Dictionary<String, Any>

            self.nomOutlet.text = leUser["nom"] as! String
            self.emailOutlet.text = leUser["email"] as! String
            self.telOutlet.text  = String(leUser["tel"] as! Int)
            self.ancienMdp = leUser["password"] as! String
            if let houlala = leUser["siret"]{
                self.patenteOutlet.text = houlala as! String
            }
        }
        
    }
    
    // 2)
    @IBAction func gererPatenteAction(_ sender: UIButton) {
        patenteOutlet.isHidden = false
        btnAfficherOutlet.isHidden = true
    }
    
    @IBAction func validerModificationAction(_ sender: UIButton) {
        updateBD()
    }
    
    
    
    func updateBD() {
        
        let urlString = ViewController.AdresseIp.monAdresse + "/api/tusers/" + String(ProfileViewController.idUser)
        var resseources2 :[String : Any]
        if let houlala = patenteOutlet.text {
            if confirmPassOutlet.text!.count == 0 {
                resseources2 = [
                    "nom": self.nomOutlet.text,
                    "email": self.emailOutlet.text! ,
                    "tel": self.telOutlet.text ,
                    "siret": patenteOutlet.text
                ]
            }else{
                resseources2 = [
                    "nom": self.nomOutlet.text,
                    "email": self.emailOutlet.text! ,
                    "tel": self.telOutlet.text ,
                    "password": self.confirmPassOutlet.text,
                    "siret": patenteOutlet.text
                ]
            }
        }else{
            if confirmPassOutlet.text!.count == 0 {
                resseources2 = [
                    "nom": self.nomOutlet.text,
                    "email": self.emailOutlet.text! ,
                    "tel": self.telOutlet.text 
                ]
            }else{
                resseources2 = [
                    "nom": self.nomOutlet.text,
                    "email": self.emailOutlet.text! ,
                    "tel": self.telOutlet.text ,
                    "password": self.confirmPassOutlet.text
                ]
            }
        }
        Alamofire.request(urlString, method: HTTPMethod.put, parameters:resseources2,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                print(value)
                self.performSegue(withIdentifier: "changement",sender: Any?.self)
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
