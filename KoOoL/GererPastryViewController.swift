//
//  GererPastryViewController.swift
//  KoOoL
//
//  Created by houssem on 5/8/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
class GererPastryViewController: UIViewController {
    var id = 0
    static var idaffectaion = 0
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionArea: UITextView!

    @IBOutlet weak var viewtoreload: UIView!
    @IBOutlet weak var nomoutlet: UITextField!
    @IBOutlet weak var telOutlet: UITextField!
    @IBOutlet weak var adresseOutlet: UITextField!
    @IBOutlet weak var smallDescOutlet: UITextView!
    
    var patisserieResponse : NSArray = []
    var patisserie : Dictionary<String, Any> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let url = ViewController.AdresseIp.monAdresse + "/api/tpastries/"+String(id)
        Alamofire.request(url).responseJSON{ response in
            self.patisserie = response.result.value as! Dictionary<String, Any>
            
            self.nomoutlet.text = self.patisserie["nom"] as? String
            self.telOutlet.text = String(self.patisserie["tel"] as! Int)
            self.adresseOutlet.text =  self.patisserie["adresse"] as? String
            
            let descriptionText = self.patisserie["iddesc"] as! Dictionary<String, Any>
            self.smallDescOutlet.text = descriptionText["smalldesc"] as? String
            self.descriptionArea.text = descriptionText["bigdesc"] as? String
            
            let iUrl = self.patisserie["image"] as! String
            let format = ".jpg"
            let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
            self.imageView.af_setImage(withURL: URL(string: imageUrl)!)
            
            
            // ce ci est pour la modification
            self.idPatisserie = self.patisserie["id"] as! Int
            self.idCommentaire = descriptionText["id"] as! Int
        }
    }

    @IBAction func btnGererProduit(_ sender: Any) {
        performSegue(withIdentifier: "gererProduits", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gererProduits"{
            let destination = segue.destination as! GererProduitViewController
            destination.id = String(self.id)
        }
    }
    
    
    //pour les modification
    var idPatisserie = 0
    var idCommentaire = 0
    @IBAction func validerAction(_ sender: UIButton) {
        //En premier lieux recuperer tout les champ
        //Ensuite mettre a jour les description
        //Enfin mettre a jour la patisserie
        //Redirection
        
        // 1)
        let nom = nomoutlet.text as? String
        let tel = telOutlet.text as? String
        let adresse  = adresseOutlet.text as? String
        let petiteDescription = smallDescOutlet.text as? String
        let grandeDescription = descriptionArea.text  as? String
        
        // 2)
        let urlString = ViewController.AdresseIp.monAdresse + "/api/tdescriptions/" + String(self.idCommentaire)
        let resseources :[String : Any] = [
            "bigdesc": grandeDescription!,
            "smalldesc": petiteDescription!,
            "id": self.idCommentaire
        ]
        Alamofire.request(urlString, method: HTTPMethod.put, parameters:resseources,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                // 3)
                let urlString2 = ViewController.AdresseIp.monAdresse + "/api/tpastries/" + String(self.idPatisserie)
                print(urlString2)
                let resseources2 :[String : Any] = [
                    "adresse": adresse!,
                    "nom": nom!,
                    "tel": tel!,
                    "iddesc": self.idCommentaire,
                    "id": self.idPatisserie
                ]
                Alamofire.request(urlString2, method: HTTPMethod.put, parameters:resseources2,encoding: JSONEncoding.default, headers: nil).responseJSON {
                    response in
                    switch response.result {
                    case .success:
                        // 4)
                        print(response)
                        self.performSegue(withIdentifier: "byebye",sender: Any?.self)
                        
                        break
                    case .failure(let error):
                        print(error)
                    }
                }
                break
            case .failure(let error):
                print(error)
            }
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
