//
//  ImageHelpViewController.swift
//  KoOoL
//
//  Created by Ahmed on 5/13/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit

class ImageHelpViewController: UIViewController {
    @IBOutlet weak var imageBack: UIImageView!
    @IBOutlet weak var lImage: UIImageView!
    let images = ["c1","c2","c3","c4"]
    static var id = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageBack.image = UIImage(named: images[ImageHelpViewController.id])
        self.lImage.image = UIImage(named: images[ImageHelpViewController.id])
        let LogOutButton = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(leAction))
        self.navigationItem.leftBarButtonItems = [LogOutButton]
    }
    @objc func leAction(){
        performSegue(withIdentifier: "rt2", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
