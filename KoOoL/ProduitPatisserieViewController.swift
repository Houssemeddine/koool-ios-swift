//
//  ProduitPatisserieViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/24/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ProduitPatisserieViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    var id = ""
    var toutLesProduit : NSArray = []
    
    struct produit {
        var id : String
        var nom : String
        var image : String
        var description : String
        init(){
            self.id = ""
            self.nom = ""
            self.description = ""
            self.image = ""
        }
    }
    var produitsSpecifiaue = [produit]()
    

    @IBOutlet weak var tab: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let url = ViewController.AdresseIp.monAdresse + "/api/tproduits"
        Alamofire.request(url).responseJSON{ response in
            self.toutLesProduit = response.result.value as! NSArray
            self.convertisseur()
            self.tab.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return produitsSpecifiaue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellprod")
        let imageview = cell!.viewWithTag(30) as! UIImageView
        let labelNom = cell!.viewWithTag(31) as! UILabel
        let labeldescription = cell!.viewWithTag(32) as! UITextView
        labelNom.text = produitsSpecifiaue[indexPath.row].nom   as? String
        let iUrl = produitsSpecifiaue[indexPath.row].image as! String
        imageview.af_setImage(withURL: URL(string: iUrl)!)
        labeldescription.text = produitsSpecifiaue[indexPath.row].description as? String
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         performSegue(withIdentifier: "warinadetail", sender: indexPath)

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "warinadetail"{
            let destination = segue.destination as! DetailProduitViewController
            let indice = sender as! IndexPath
            let id = produitsSpecifiaue[indice.row].id
            let tempid = Int(id)!
            DetailProduitViewController.id = tempid
        }
    }
    
    
    
    //ce convertisseur nous permet de selectionner que les elements de notre patisserie
    func convertisseur(){
        
        for elem in toutLesProduit{
            let element = elem as! Dictionary<String, Any>
            let ourourou2 = element["idpastry"] as! Dictionary<String, Any>
            let idTemp = String(ourourou2["id"] as! Int)
           
            if idTemp == self.id{
                var temp = produit()
                let id = element["id"] as! Int
                temp.id = String(id)
                temp.nom = element["nom"] as! String
                let ourourou = element["iddesc"] as! Dictionary<String, Any>
                temp.description = ourourou["smalldesc"] as! String
                let iUrl = element["image"] as! String
                let format = ".jpg"
                temp.image = ViewController.AdresseIp.adresseImage + iUrl + format
                self.produitsSpecifiaue.append(temp)
            }            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
