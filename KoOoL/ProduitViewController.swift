//
//  ProduitViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/10/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import Alamofire

class ProduitViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource  {

    
    
    @IBOutlet weak var tabProduit: UITableView!
    var produits : NSArray = []
    
    // cette methode va nous permettre d`afficher les icone en bas
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem (title: "Prodiut" , image: UIImage(named: "iconproduit") , tag: 2)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        convertisseur()
        var longuer = rechercheFild.text as! String
        collectionViewRecherche.isHidden = true
        //rechqrger tt les produit du site
        let url = ViewController.AdresseIp.monAdresse + "/api/tproduits"
        Alamofire.request(url).responseJSON{ response in
            self.produits = response.result.value as! NSArray
            self.tabProduit.reloadData()
        }
        
        // pour le bouton rafraichir
        let refrech = UIBarButtonItem(barButtonSystemItem: .refresh  , target: self, action: #selector(leAction))
        self.navigationItem.rightBarButtonItems = [refrech]

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return produits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellProdit")
        let imageview = cell!.viewWithTag(41) as! UIImageView
        let labelNom = cell!.viewWithTag(42) as! UILabel
        let labelpatisserie = cell!.viewWithTag(43) as! UILabel
        let labelprix = cell!.viewWithTag(44) as! UILabel
        
        let produit = produits[indexPath.row] as! Dictionary<String, Any>
        
        let iUrl = produit["image"] as! String
        let format = ".jpg"
        let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
        imageview.af_setImage(withURL: URL(string: imageUrl)!)
        
        labelNom.text = produit["nom"] as? String
        labelprix.text = String(produit["prix"] as! Int)
        
        let patisserie = produit["idpastry"] as! Dictionary<String, Any>
        labelpatisserie.text = patisserie["nom"] as? String
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ilaDetail", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ilaDetail"{
            let destination = segue.destination as! DetailProduitViewController
            let indice = sender as! IndexPath
            let patiss = produits[indice.row] as! Dictionary<String, Any>
            DetailProduitViewController.id = patiss["id"] as! Int
        }
        if segue.identifier == "endah1"{
            let destination = segue.destination as! DetailViewController
            let indice = sender as! IndexPath
            let prod = arrayFinal[indice.row]
            destination.id = prod.id
        }
        if segue.identifier == "endah2"{
            let destination = segue.destination as! DetailProduitViewController
            let indice = sender as! IndexPath
            let prod = arrayFinal[indice.row]
            DetailProduitViewController.id = prod.id
        }
    }
    //tt ce qui est lq nou sert de recheche
    
    @IBOutlet weak var collectionViewRecherche: UICollectionView!
    struct patisserierecherche {
        var id : Int
        var nom : String
        var image : String
        var tel : Int
        var adresse : String
        var description : String
        init(){
            self.id = 0
            self.nom = ""
            self.image = ""
            self.adresse = ""
            self.description = ""
            self.tel = 0
        }
    }
    struct produitrecherche {
        var id : Int
        var prix : Int
        var nom : String
        var image : String
        var type : String
        var description : String
        init(){
            self.id = 0
            self.prix = 0
            self.nom = ""
            self.image = ""
            self.description = ""
            self.type = ""
        }
    }
    var arrayPat = [patisserierecherche]()
    var arrayProduit = [produitrecherche]()
    var patisseries : NSArray = []

    
    func convertisseur() {
        let url1 = ViewController.AdresseIp.monAdresse + "/api/tpastries"
        Alamofire.request(url1).responseJSON{ response in
            self.patisseries = response.result.value as! NSArray
            for elem in self.patisseries{
                let element = elem as! Dictionary<String, Any>
                var var1 = patisserierecherche()
                var1.id = element["id"] as! Int
                var1.nom = element["nom"] as! String
                var1.image = element["image"] as! String
                var1.adresse = element["adresse"] as! String
                var1.tel = element["tel"] as! Int
                let ourourou2 = element["iddesc"] as! Dictionary<String, Any>
                var1.description = ourourou2["bigdesc"] as! String
                self.arrayPat.append(var1)
            }
        }
        let url2 = ViewController.AdresseIp.monAdresse + "/api/tproduits"
        Alamofire.request(url2).responseJSON{ response in
            self.produits = response.result.value as! NSArray
            for elem in self.produits{
                let element = elem as! Dictionary<String, Any>
                var var1 = produitrecherche()
                var1.id = element["id"] as! Int
                var1.nom = element["nom"] as! String
                var1.image = element["image"] as! String
                var1.type = element["type"] as! String
                var1.prix = element["prix"] as! Int
                let ourourou2 = element["iddesc"] as! Dictionary<String, Any>
                var1.description = ourourou2["bigdesc"] as! String
                self.arrayProduit.append(var1)
            }


        }
        
    }
    
    @IBOutlet weak var rechercheFild: UITextField!
    @IBOutlet weak var collectionRecherche: UICollectionView!
    
    var arrayPatrecherche = [patisserierecherche]()
    var arrayProduitrecherche = [produitrecherche]()
    
    struct listerechercheElem {
        var id : Int
        var prix : Int
        var nom : String
        var image : String
        var type : String
        var description : String
        var tel : Int
        var adresse : String
        var resultatRecherche : String
        init(){
            self.id = 0
            self.prix = 0
            self.nom = ""
            self.tel = 0
            self.adresse = ""
            self.image = ""
            self.description = ""
            self.type = ""
            self.resultatRecherche = ""
        }
    }
    var arrayFinal = [listerechercheElem]()
    func concatResultat(liste1 : Array<patisserierecherche>, liste2 : Array<produitrecherche> ) -> Array<listerechercheElem> {
        var resPat = [listerechercheElem]()
        for pat in liste1{
            var temp1 = listerechercheElem()
            temp1.resultatRecherche = "Patisserie"
            temp1.id = pat.id
            temp1.prix = 0
            temp1.nom = pat.nom
            temp1.tel = pat.tel
            temp1.adresse = pat.adresse
            temp1.image = pat.image
            temp1.description = pat.description
            temp1.type = ""
            resPat.append(temp1)
        }
        for prod in liste2{
            var temp2 = listerechercheElem()
            temp2.resultatRecherche = "Produit"
            temp2.id = prod.id
            temp2.prix = prod.prix
            temp2.nom = prod.nom
            temp2.tel = 0
            temp2.adresse = ""
            temp2.image = prod.image
            temp2.description = prod.description
            temp2.type = prod.type
            resPat.append(temp2)
            
        }
        return resPat
    }
    
    
    @IBAction func btnRechercher(_ sender: UIButton) {
        arrayPatrecherche.removeAll()
        arrayProduitrecherche.removeAll()
        collectionRecherche.reloadData()
        let textRecher = rechercheFild.text as! String
        collectionViewRecherche.isHidden = false
        if textRecher.count==0{
            collectionViewRecherche.isHidden = true
        }
        arrayPatrecherche = rechercherpqty(sub: textRecher)
        arrayProduitrecherche = rechercherproduit(sub: textRecher)
        self.arrayFinal = concatResultat(liste1: arrayPatrecherche, liste2: arrayProduitrecherche)
        collectionRecherche.reloadData()
    }
    
    @IBAction func rechercheAjaax(_ sender: UITextField) {
        print(rechercheFild.text)
        arrayPatrecherche.removeAll()
        arrayProduitrecherche.removeAll()
        collectionRecherche.reloadData()
        let textRecher = rechercheFild.text as! String
        collectionViewRecherche.isHidden = false
        if textRecher.count==0{
            collectionViewRecherche.isHidden = true
        }
        arrayPatrecherche = rechercherpqty(sub: textRecher)
        arrayProduitrecherche = rechercherproduit(sub: textRecher)
        self.arrayFinal = concatResultat(liste1: arrayPatrecherche, liste2: arrayProduitrecherche)
        collectionRecherche.reloadData()
    }
    func rechercherpqty(sub : String) -> Array<patisserierecherche> {
        var resPat = [patisserierecherche]()
        for pat in self.arrayPat{
            if pat.nom.contains(sub){
                resPat.append(pat)
            }
            if pat.description.contains(sub){
                resPat.append(pat)
            }
            if pat.adresse.contains(sub){
                resPat.append(pat)
            }
            let telTemp = String(pat.tel)
            if telTemp == sub{
                resPat.append(pat)
            }
        }
    
        return resPat
    }
    
    func rechercherproduit(sub : String) -> Array<produitrecherche> {
        var resPat = [produitrecherche]()
        for pat in self.arrayProduit{
            if pat.nom.contains(sub){
                resPat.append(pat)
            }
            if pat.description.contains(sub){
                resPat.append(pat)
            }
            if pat.type.contains(sub){
                resPat.append(pat)
            }
            let telTemp = String(pat.prix)
            if telTemp==sub{
                resPat.append(pat)
            }
        }
        return resPat
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayFinal.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellrecher", for: indexPath)
        let image = cell.viewWithTag(53) as! UIImageView
        let nomLabel = cell.viewWithTag(54) as! UILabel
        let typeLabel = cell.viewWithTag(55) as! UILabel
        // arrondir les bor de la cellule
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        
        if arrayFinal[indexPath.row].resultatRecherche == "Patisserie"{
            let iUrl = arrayFinal[indexPath.row].image
            let format = ".jpg"
            let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
            image.af_setImage(withURL: URL(string: imageUrl)!)
            nomLabel.text = arrayFinal[indexPath.row].nom
            typeLabel.text = arrayFinal[indexPath.row].resultatRecherche
            cell.backgroundColor = UIColor(hexString: "#FF4081 ")
        }else{
            let iUrl = arrayFinal[indexPath.row].image
            let format = ".jpg"
            let imageUrl = ViewController.AdresseIp.adresseImage + iUrl + format
            image.af_setImage(withURL: URL(string: imageUrl)!)
            nomLabel.text = arrayFinal[indexPath.row].nom
            typeLabel.text = arrayFinal[indexPath.row].resultatRecherche
            cell.backgroundColor = UIColor(hexString: "#F8BBD0 ")
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arrayFinal[indexPath.row].resultatRecherche == "Patisserie"{
            performSegue(withIdentifier: "endah1", sender: indexPath)
        }else{
            performSegue(withIdentifier: "endah2", sender: indexPath)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func leAction(){
        convertisseur()
        var longuer = rechercheFild.text as! String
        collectionViewRecherche.isHidden = true
        //rechqrger tt les produit du site
        let url = ViewController.AdresseIp.monAdresse + "/api/tproduits"
        Alamofire.request(url).responseJSON{ response in
            self.produits = response.result.value as! NSArray
            self.tabProduit.reloadData()
        }
    }
}
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
