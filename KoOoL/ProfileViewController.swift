//
//  ProfileViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/10/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class ProfileViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var tabFav: UICollectionView!
    
    static var idUser = 0
    
    @IBOutlet weak var btnModicfierProfil: UIButton!
    @IBOutlet weak var btnVotrepatisserie: UIButton!
    @IBOutlet weak var btnLogOut: UIButton!
    @IBOutlet weak var nomFild: UILabel!
    @IBOutlet weak var telFild: UILabel!
    @IBOutlet weak var emailFild: UILabel!
    @IBOutlet weak var labelFaverie: UILabel!
    @IBOutlet weak var logoFavorie: UIImageView!
    @IBOutlet weak var seConnecterBtn: UIButton!
    
    static var connectiii = true
    var allPatisseries : NSArray = []
    struct elem {
        var id : Int
        var nom : String
        var image : String
        init(){
            self.id = 0
            self.nom = ""
            self.image = ""
        }
    }
    var arrayElement = [elem]()
    var estDejaAbonne = false
    
    // cette methode va nous permettre d`afficher les icone en bas
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem (title: "Profile" , image: UIImage(named: "iconprofile") , tag: 3)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let LogOutButton = UIBarButtonItem(title: "LogOut", style: .done, target: self, action: #selector(leAction))

        if ProfileViewController.idUser != 0 && ProfileViewController.connectiii{
            self.navigationItem.rightBarButtonItems = [LogOutButton]
        }
        // Do any additional setup after loading the view.
        if ProfileViewController.idUser == 0 {
            //btnLogOut.isHidden = true
            btnVotrepatisserie.isHidden = true
            btnModicfierProfil.isHidden = true
            self.logoFavorie.isHidden = true
            self.labelFaverie.isHidden = true
            seConnecterBtn.isHidden = false
        }
        //Recuperation des donnees du core data
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let reqete = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        reqete.returnsObjectsAsFaults = false
        do {
            let resultats = try context.fetch(reqete)
            if resultats.count > 0 && ProfileViewController.idUser != 0{
                for r in resultats as! [NSManagedObject]{
                    if let nom = r.value(forKey: "nom") as? String{
                        nomFild.text = nom
                    }
                    if let tel = r.value(forKey: "tel") as? String{
                        telFild.text = tel
                    }
                    if let email = r.value(forKey: "email") as? String{
                        emailFild.text = email
                    }
                    //btnLogOut.isHidden = false
                    btnModicfierProfil.isHidden = false
                    btnVotrepatisserie.isHidden = false
                }
            }
            if resultats.count == 0{
                nomFild.text = "Vous devez vous connecter"
                telFild.text = "Vous devez vous connecter"
                emailFild.text = "Vous devez vous connecter"
            }
        } catch  {
            print("eror dans le get de l appli")
        }
        
        //  affichage des patisserie favoris
        let url = ViewController.AdresseIp.monAdresse + "/api/tpastries"
        Alamofire.request(url).responseJSON{ response in
            self.allPatisseries = response.result.value as! NSArray
            self.Listefav(allPatisserie: self.allPatisseries)
            self.tabFav.reloadData()
        }
    }
    
    @IBAction func logOutAction(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistenceContainer = appDelegate.persistentContainer
        let managedContext = persistenceContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            /*let resultat = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            managedContext.delete(resultat[1])
            try managedContext.save()*/
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
            nomFild.text = "Vous devez vous connecter"
            telFild.text = "Vous devez vous connecter"
            emailFild.text = "Vous devez vous connecter"
            //btnLogOut.isHidden = true
            btnModicfierProfil.isHidden = true
            btnVotrepatisserie.isHidden  = true
            ProfileViewController.idUser = 0
            ProfileViewController.connectiii = false
            performSegue(withIdentifier: "logetout", sender: self)
        }catch let nsError as NSError
        {
            print(nsError.userInfo)
        }

    }
    // remplisage des favories
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayElement.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "celuleFavorite", for: indexPath) as! MyCollectionViewCell
        let image = cell.viewWithTag(39) as! UIImageView
        let nomLabel = cell.viewWithTag(38) as! UILabel
        nomLabel.text = arrayElement[indexPath.row].nom
        image.af_setImage(withURL: URL(string: arrayElement[indexPath.row].image)!)
        
        cell.myButton.tag = indexPath.row
        
        cell.myButton.addTarget(self, action: #selector(
            self.sedesabonnee(sender:)), for: .touchUpInside)
        
        return cell
    }
    @objc func sedesabonnee(sender : UIButton){
        desabonni(id: arrayElement[sender.tag].id)
        arrayElement.remove(at: sender.tag)
        
        self.tabFav.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goDetail", sender: indexPath)
    }
    var idDeMaPatisserie = 0
    //iciiiiiiii\
    static var userConnecte = userPatisserie()
    
    @IBAction func maPatisserieAction(_ sender: UIButton) {
        let tempIdUser = String(ProfileViewController.idUser)
        let url = ViewController.AdresseIp.monAdresse + "/api/tusers/" + tempIdUser
        Alamofire.request(url).responseJSON{ response in
            let leUser = response.result.value as! Dictionary<String, Any>
            if let houlala = leUser["idpastry"]{
                let tempFinal1 = leUser["idpastry"] as! Dictionary<String, Any>
                let tempFinal2 = tempFinal1["id"] as! Int
                self.idDeMaPatisserie = tempFinal2
                GererPastryViewController.idaffectaion = tempFinal2
                self.performSegue(withIdentifier: "modifierSaPatisserie", sender: self)
            }else{
                self.performSegue(withIdentifier: "addNewPatisserie", sender: self)
            }
            
            // ajouter le user du serveur a mon aplication
            ProfileViewController.userConnecte.id = leUser["id"] as! Int
            ProfileViewController.userConnecte.nom = leUser["nom"] as! String
            ProfileViewController.userConnecte.email = leUser["email"] as! String
            ProfileViewController.userConnecte.tel = leUser["tel"] as! Int
            //ProfileViewController.userConnecte.image = leUser["image"] as! String
            ProfileViewController.userConnecte.password = leUser["password"] as! String
            ProfileViewController.userConnecte.role = leUser["role"] as! String
            //ProfileViewController.userConnecte.idpastry = leUser["idpastry"] as! Int
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goDetail"{
            let destination = segue.destination as! DetailViewController
            let indice = sender as! IndexPath
            destination.id = arrayElement[indice.row].id
        }
        if segue.identifier == "modifierSaPatisserie"{
            let destination = segue.destination as! GererPastryViewController
            destination.id = idDeMaPatisserie
        }
    }
    @IBAction func btnDelete(_ sender: Any) {
    }
    
    func Listefav(allPatisserie : NSArray) {
        for patisserie in allPatisserie{
            let patisserieTemp = patisserie as! Dictionary<String, Any>
            let pattisserie = patisserieTemp["id"] as! Int
            let urlString = ViewController.AdresseIp.monAdresse + "/verifierAbonnerPastry"
            Alamofire.request(urlString,parameters: [
                "pastry": pattisserie,
                "user": ProfileViewController.idUser
                ]).responseJSON{ response in
                    let tempResultat = response.result.value as! Dictionary<String, Bool>
                    let aaa  = tempResultat["subscribe"] as! Bool
                    if aaa{
                        //ajouter l id de la patisserie dans la liste
                        let url = ViewController.AdresseIp.monAdresse + "/api/tpastries/"+String(pattisserie)
                        Alamofire.request(url).responseJSON{ response in
                            let houlala = response.result.value as! Dictionary<String, Any>
                            var temp = elem()
                            temp.id = pattisserie as! Int
                            temp.nom = houlala["nom"] as! String
                            let iUrl = houlala["image"] as! String
                            let format = ".jpg"
                            temp.image = ViewController.AdresseIp.adresseImage + iUrl + format
                            self.arrayElement.append(temp)
                            //pour savoir est ce auil a deja un abonnemet ou nn
                            self.estDejaAbonne = true
                            self.tabFav.reloadData()
                        }
                    }
                    self.labelFaverie.isHidden = self.estDejaAbonne
                    self.logoFavorie.isHidden = self.estDejaAbonne
                    self.tabFav.reloadData()
            }
        }
        
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    struct userPatisserie : Encodable  {
        var id : Int
        var nom : String
        var email : String
        var image : String
        var password : String
        var role : String
        var tel : Int
        var idpastry : Int
        init(){
            self.id = 0
            self.nom = ""
            self.email = ""
            self.image = ""
            self.password = ""
            self.role = ""
            self.tel = 0
            self.idpastry = 0
        }
    }
    
    @objc func leAction(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistenceContainer = appDelegate.persistentContainer
        let managedContext = persistenceContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            /*let resultat = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
             managedContext.delete(resultat[1])
             try managedContext.save()*/
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
            nomFild.text = "Vous devez vous connecter"
            telFild.text = "Vous devez vous connecter"
            emailFild.text = "Vous devez vous connecter"
            //btnLogOut.isHidden = true
            btnModicfierProfil.isHidden = true
            btnVotrepatisserie.isHidden = true
            ProfileViewController.idUser = 0
            ProfileViewController.connectiii = false
            performSegue(withIdentifier: "logetout", sender: self)
        }catch let nsError as NSError
        {
            print(nsError.userInfo)
        }
    }
    
    func desabonni(id : Int) {
        let urlPostProduit = ViewController.AdresseIp.monAdresse + "/desabonnerPastry"
        var params = [
            "iduser": ProfileViewController.idUser,
            "idpastry": id,
            ] as [String : Any]
        Alamofire.request(urlPostProduit, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON{
            response in
            switch response.result {
            case .success:
                break
            case .failure(let error):
                print("ajoutProduit class, jason responce eroor ")
                print(error)
            }
        }
    }
}

class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet var myButton: UIButton!
}
