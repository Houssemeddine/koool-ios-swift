//
//  RegisterViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/23/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import AlamofireImage

class RegisterViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {

    @IBOutlet weak var profilepic: UIButton!
    @IBOutlet weak var nomInput: UITextField!
    @IBOutlet weak var prenomInput: UITextField!
    @IBOutlet weak var telInput: UITextField!
    @IBOutlet weak var mailInput: UITextField!
    @IBOutlet weak var passwordInput1: UITextField!
    @IBOutlet weak var passwordInput2: UITextField!
    @IBOutlet weak var sierteInput: UITextField!
    @IBOutlet weak var btnsiert: UIButton!
    
    let picker = UIImagePickerController()
    
    
    var ttLesUsertemp : NSArray = []
    
    @IBAction func setProfilPic(_ sender: UIButton) {
        profilepic.setImage(UIImage(named: "0"), for: .normal)
        picker.allowsEditing = false //2
        picker.sourceType = .photoLibrary //3
        present(picker, animated: true, completion: nil)//
    }
    
    
    @IBAction func patenteBtn(_ sender: UIButton) {
        sierteInput.isHidden = false
        btnsiert.isHidden = true
    }
    @IBAction func btnAction(_ sender: UIButton) {
        let nom = nomInput.text as? String
        let prenom = prenomInput.text as? String
        let tel = telInput.text as? String
        let mail = mailInput.text as? String
        let password = passwordInput1.text as? String
        let passTemp = passwordInput2.text as? String
        var sier = sierteInput.text as? String
        
        
        if nom!.count==0{
            nomInput.backgroundColor = UIColor.red
        }else{
            if prenom!.count == 0 {
                prenomInput.backgroundColor = UIColor.red
            }else{
                if tel!.count == 0 {
                    telInput.backgroundColor = UIColor.red
                }else{
                    if mail!.count == 0 && !isValidEmail(testStr: mail!){
                         mailInput.backgroundColor = UIColor.red
                    }else {
                        if password!.count == 0 {
                            passwordInput1.backgroundColor = UIColor.red
                            passwordInput2.backgroundColor = UIColor.red
                        }else{
                            // debut du traitement
                            if sier!.count == 0{
                                sier = "annus"
                            }
                            
                             if password == passTemp {
                             let urlString = ViewController.AdresseIp.monAdresse + "/api/tusers"
                             Alamofire.request(urlString, method: .post, parameters: [
                             "email": mail!,
                             "nom": nom! + prenom!,
                             "password": password!,
                             "role": "Manager",
                             "tel": tel!,
                             "siret":sier!
                             ],encoding: JSONEncoding.default, headers: nil).responseData {
                             response in
                             switch response.result {
                             case .success:
                             self.ajout()
                             self.performSegue(withIdentifier: "passage",sender: Any?.self)
                             
                             break
                             case .failure(let error):
                             print("regester class, jason responce eroor ")
                                print(error)
                             }
                             }
                             }
                             else{
                             let alert = UIAlertController(title: "Attention", message: "les mots de passe ne sont pas identiques", preferredStyle: .alert)
                             alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                             self.present(alert, animated: true)
                             }
                            //fin traitement
                        }
                    }
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // au debut la sieretdoit etre masquer
        self.sierteInput.isHidden = true
        picker.delegate = self

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //pour le upload des photos
    func imagePickerController(
        picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        /*let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        myImageView.contentMode = .ScaleAspectFit //3
        myImageView.image = chosenImage //4
        dismissViewControllerAnimated(true, completion: nil) //5
*/
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func ajout(){
        //testtesttesttest
        let url = ViewController.AdresseIp.monAdresse + "/api/tusers"
        Alamofire.request(url).responseJSON{ response in
            self.ttLesUsertemp = response.result.value as! NSArray
        
        let emailFildtemp = self.mailInput.text as! String
        let passFildtemp = self.passwordInput1.text as! String
        
        for elem in self.ttLesUsertemp{
            let element = elem as! Dictionary<String, Any>
            let email = element["email"] as! String
            let pass = element["password"] as! String
            
            if email == emailFildtemp &&  pass == passFildtemp{
                //Insersion dans le core data
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                let newUser = NSEntityDescription.insertNewObject(forEntityName: "User", into: context)
                newUser.setValue(String(element["id"] as! Int),forKey:"id")
                newUser.setValue(element["nom"] as! String,forKey:"nom")
                newUser.setValue(email,forKey:"email")
                newUser.setValue(pass,forKey:"password")
                do {
                    try newUser.setValue(element["siret"] as? String,forKey:"sieret")
                } catch  {
                    print("--------")
                }
                newUser.setValue(String(element["tel"] as! Int),forKey:"tel")
                ProfileViewController.idUser = element["id"] as! Int

                /*
                 let elemtemmp = element["idpastry"] as! Dictionary<String, Any>
                 newUser.setValue(elemtemmp["id"] as! String,forKey:"idpastry")
                 */
                do {
                    try context.save()
                    print("context saved !")
                } catch  {
                    print("Erreur ! lors de la sauvegarde ")
                }
                //testtesttesttest
            }
            }
            
        }
    }
}

