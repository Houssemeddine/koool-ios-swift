//
//  ViewController.swift
//  KoOoL
//
//  Created by Ahmed on 4/10/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import CoreData
class ViewController: UIViewController, FBSDKLoginButtonDelegate{
    public struct AdresseIp {
        static let monAdresse = "http://41.226.11.252:1180/koool/mobileService/web/app.php"
        static let adresseImage = "http://41.226.11.252:1180/koool/mobileService/web/upload/"
    }
    
    
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print("connecté")
            print(result)
        }else{
            print("vous n'etes paas connecté")
        }
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("tu t'es deconnectè")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loginButton = FBSDKLoginButton();
        loginButton.delegate = self
        
        loginButton.center = self.view.center;
        self.view.addSubview(loginButton);
        if verifConnec(){
            performSegue(withIdentifier: "bravoo", sender: self)
        }
    }
    
    func verifConnec() -> Bool {
        // cette methoed consiste a voir si l utilisateur est connect au paravant ou nn
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let reqete = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        reqete.returnsObjectsAsFaults = false
        do {
            let resultats = try context.fetch(reqete)
            if resultats.count > 0{
                for r in resultats as! [NSManagedObject]{
                    if let id = r.value(forKey: "id") as? String{
                        ProfileViewController.idUser = Int(id)!
                    }
                }
                return true
            }
        } catch  {
            print("eror dans le get de l appli")
        }
        return false
    }
    

}

