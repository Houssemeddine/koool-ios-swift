//
//  ViewController1.swift
//  KoOoL
//
//  Created by Ahmed on 5/13/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn1(_ sender: Any) {
        let storybord = UIStoryboard(name: "Main", bundle: nil)
        ImageHelpViewController.id = 0
        performSegue(withIdentifier: "g1", sender: self)
    }
    @IBAction func btn2(_ sender: Any) {
        ImageHelpViewController.id = 1
        performSegue(withIdentifier: "g1", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
