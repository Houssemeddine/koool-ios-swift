//
//  ViewController2.swift
//  KoOoL
//
//  Created by Ahmed on 5/13/19.
//  Copyright © 2019 AhmedHoussem_Organization. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn3(_ sender: Any) {
        ImageHelpViewController.id = 2
        performSegue(withIdentifier: "g2", sender: self)
    }
    
    @IBAction func btn4(_ sender: Any) {
        ImageHelpViewController.id = 3
        performSegue(withIdentifier: "g2", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
